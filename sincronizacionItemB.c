#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>

sem_t uno, dos, tre, cua, cin, sei, sie, aux;

int cont=1;

void* thread1(void* arg){
	while(0 < cont){
		sem_wait(&uno);
		printf("Pienso\n");
		cont--;
		sem_post(&dos);
		sem_post(&tre);
		sem_post(&cua);
		
	}
}
void* thread2(void* arg){ 
	while(0 < cont){
		sem_wait(&dos);
		printf("Mientras lavo los platos. ");
		sem_post(&cin);
	}
}
void* thread3(void* arg){
	while(0 < cont){
		sem_wait(&tre);
		printf("Mientras limpio el piso. ");
		sem_post(&cin);
	}
}
void* thread4(void* arg){
	while(0 < cont){
		sem_wait(&cua);
		printf("Mientras riego las plantas. ");
		sem_post(&cin);
	}
}
void* thread5(void* arg){
	while(0 < cont){
		sem_wait(&cin);
		sem_wait(&cin);
		sem_wait(&cin);
		printf("\nExisto!\n");
		sem_post(&sei);
	}
}
void* thread6(void* arg){
	while(0 < cont){
		sem_wait(&sei);
		printf("Hablar, ");
		sem_post(&sie);
	}
}
void* thread7(void* arg){
	while(0 < cont){
		sem_wait(&sie);
		printf("tomar una decisión.\n");
		sem_post(&uno);
	}
}

int main(){

	pthread_t t1, t2, t3, t4, t5, t6, t7;
	sem_init(&uno,0,1);
	sem_init(&dos,0,0);
	sem_init(&tre,0,0);
	sem_init(&cua,0,0);
	sem_init(&sei,0,0);
	sem_init(&sie,0,0);
	sem_init(&aux,0,0);

	pthread_create(&t1, NULL, *thread1, NULL);
	pthread_create(&t2, NULL, *thread2, NULL);
	pthread_create(&t3, NULL, *thread3, NULL);
	pthread_create(&t4, NULL, *thread4, NULL);
	pthread_create(&t5, NULL, *thread5, NULL);
	pthread_create(&t6, NULL, *thread6, NULL);
	pthread_create(&t7, NULL, *thread7, NULL);

	sem_destroy(&uno);
	sem_destroy(&dos);
	sem_destroy(&tre);
	sem_destroy(&cua);
	sem_destroy(&sei);
	sem_destroy(&sie);
	sem_destroy(&aux);

	pthread_exit(NULL);
	return 0;
}
