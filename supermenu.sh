#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
	red=`tput setaf 1`;
	green=`tput setaf 2`;
	blue=`tput setaf 4`;
	bg_blue=`tput setab 4`;
	reset=`tput sgr0`;
	bold=`tput setaf bold`;

#------------------------------------------------------
# TODO: Completar con su path
#------------------------------------------------------
proyectoActual=/home/$USER/tp-sor

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t S  U  P  E  R  -  M  E  N  U ";
    
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual"; 
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver estado del proyecto";
    echo -e "\t\t\t b.  Guardar cambios";
    echo -e "\t\t\t c.  Actualizar repo";
    echo -e "\t\t\t d.  Buscar palabra en directorio";
    echo -e "\t\t\t e.  Ver archivos";
    echo -e "\t\t\t f.  Abrir en terminal";        
    echo -e "\t\t\t g.  Abrir en carpeta"; 
    echo -e "\t\t\t h.  Ejecutar archivo.c con time";
    echo -e "\t\t\t i.  Ejecutar nano";
    echo -e "\t\t\t j.  Ejecutar vim";
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue}${red}${bold}--------------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red}${bold}--------------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
	while true; do
    	read respuesta;
    	case $respuesta in
        	[Nn]* ) break;;
       		[Ss]* ) eval $1
			break;;
        	* ) echo "Por favor tipear S/s ó N/n.";;
    	esac
	done
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {

    imprimir_encabezado "\tOpción a.  Ver estado del proyecto";
	echo "---------------------------"        
	echo "¿Desea ver si es necesario realizar algún commit?"
    decidir "cd $proyectoActual; git status";
    echo "---------------------------"        
	echo "Desea realizar un git fetch origin?"
	decidir "cd $proyectoActual; git fetch origin"
	echo "---------------------------" 
	echo "¿Desea realizar un git log HEAD?"
	decidir "cd $proyectoActual; git log HEAD..origin/master --oneline"
}

b_funcion () {

    imprimir_encabezado "\tOpción b.  Guardar cambios";
	echo "¿Desea guardar cambios?";
    decidir "cd $proyectoActual; git add -A";
    echo "Ingrese mensaje para el commit:";
    read mensaje;
	echo "---------------------------"
	echo "Mensaje: $mensaje. Continuar?";
    decidir "cd $proyectoActual; git commit -m \"$mensaje\"";
	echo "---------------------------"
	echo "¿Desea realizar un git push?";
    decidir "cd $proyectoActual; git push";
}

c_funcion () {

    imprimir_encabezado "\tOpción c.  Actualizar repo";
	echo "¿Desea realizar un git pull?";
    decidir "cd $proyectoActual; git pull";   	 
}


d_funcion () {

	imprimir_encabezado "\tOpción d. Buscar palabra en directorio";
	echo "¿Desea buscar?";
	decidir estaArchivo;
}

e_funcion () {

	imprimir_encabezado "\tOpción e. Ver archivos";
	echo "¿Desea ver todos los archivos?";
	decidir verContenido;
}

f_funcion () {

	imprimir_encabezado "\tOpción f.  Abrir en terminal"; 
	echo "¿Desea abrir?";       
	decidir "cd $proyectoactual; xterm &";
}

g_funcion () {

	imprimir_encabezado "\tOpción g.  Abrir en carpeta"; 
	echo "¿Desea abrir?";       
	decidir "gnome-open $proyectoActual &";
}


h_funcion(){

	imprimir_encabezado "\tOpción h. Ejecutar archivo.c con time";
	echo "Escriba el nombre del archivo en C que desea ejecutar";
	read respuesta
	echo "¿Desea ejecutar?";
	decidir "gcc $respuesta.c -o ejecutable -lpthread";
	time ./ejecutable

}

i_funcion(){
	
	imprimir_encabezado "\tOpción i. Ejecutar nano";
	echo "¿Desea ejecutar?";
	decidir "nano";
}

j_funcion(){
	imprimir_encabezado "\tOpción j. Ejecutar vim";
	echo "¿Desea ejecutar?";
	decidir "vim";
}

#------------------------------------------------------
# TODO: Completar con el resto de ejercicios del TP, una funcion por cada item
#------------------------------------------------------

estadoDelRepo () { 

	git fetch origin #git fetch origin #Toma los datos del repo remoto y los trae al local.

	git log HEAD..origin/master --oneline

	imprimir_encabezado "\t\t¡ATENCIÓN!";
	echo -e "¿Desea verificar si hay actualizaciones disponibles?";
	decidir "cd $proyectoActual; hacer_pull";
	esperar;
}

hacer_pull () {
	imprimir_encabezado "\t\tActualizar";
	git pull
}



estaArchivo () { #Indica si se encuentra una palabra en especifica dentro de un directorio proporcionado
			#En caso positivo arroja un .txt con el nombre de los archivos donde se encuentra dicha palabra
	
	echo "Ingrese la palabra";
	read palabra;
	echo "Ingrese el directorio donde buscar";
	echo "En el formato: '/home/usuario/carpeta'"; #ej: /home/usuario/tp-sor
	read directorio;
	grep -iq $palabra $directorio/* # -i para ignorar mayusculas y minusculas 
	# q para no mostrar las demas lineas con las ubicaciones de los archivos ni los otros mensajes arrojados por grep(quiet)
	if [ $? -eq 0 ]; then #si $? arroja un 0 significa que grep encontró la palabra, caso contrario no (o que hubo error) 
		echo "La palabra $palabra se encuentra dentro del directorio $directorio";
		grep -i $palabra $directorio/* >> listaDeBusqueda.txt #el resultado es trasladado a un .txt
		echo "¿Desea ver el resultado de busqueda?";
		decidir "cat listaDeBusqueda.txt";
	else	
		echo "La palabra $palabra no se encuentra dentro del directorio $directorio";
	fi
}

verContenido() { #Imprime por pantalla una lista con todos los archivos contenidos en un determinado directorio
	
	echo "Ingrese directorio";
	echo "Ejemplo: /home/usuario/carpeta"; #por ej /home/usuario/Descargas
	read direccion;
	cd /; 
	cd $direccion;
	ls -la > listaDeArchivos.txt;
	cat listaDeArchivos.txt;	
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------

estadoDelRepo;
while  true
do
	
	# 1. mostrar el menu
    	imprimir_menu;
    	# 2. leer la opcion del usuario
    	read opcion;
    
    	case $opcion in
        	a|A) a_funcion;;
        	b|B) b_funcion;;
        	c|C) c_funcion;;
        	d|D) d_funcion;;
        	e|E) e_funcion;;
        	f|F) f_funcion;;
        	g|G) g_funcion;; 
		h|H) h_funcion;;
		i|I) i_funcion;;
		j|J) j_funcion;;
        	q|Q) break;;
        	*) malaEleccion;;
    	esac
    	esperar;
done
