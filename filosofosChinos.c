
//FILOSOFOS CHINOS

#include <stdio.h> //Librería standard input output
#include <unistd.h> //Librería para utilizar el sleep
#include <pthread.h> //librería para utilizar pthread
#include <semaphore.h> //Librería para utilizar semáforos
#include <time.h> //Inicializa el tiempo

//declaraciones

int P;
pthread_mutex_t mutex;
sem_t P1, P2, P3, P4, P5;

static void do_nothing (int microseconds, char* mensaje){
	usleep(microseconds);
	printf (" %s ", mensaje);
}

static void* Filosofo1 (void* platos){ //cantPlatos representa la cantidad de platos que va a comer el filosofo

	int P = *((int *) platos);	
	int i = 0; //Comienzo desde cero hasta n para repetir el proceso
	while (i < P){
		//Estado pensando
		printf("\nSoy el filosofo 1: Pensando...");
		do_nothing(2,"\nSoy el filosofo 1: Eureka!");
		//Sección crítica: Cuando agarra palillos
		//Estado agarrando palillo izquierdo
		pthread_mutex_lock(&mutex);
		sem_wait(&P1);
		printf("\nSoy el filosofo 1: Agarrando palillo izquierdo.");
		pthread_mutex_unlock(&mutex);
		//Estado agarrando palillo derecho
		pthread_mutex_lock(&mutex);
		sem_wait(&P2);
		printf("\nSoy el filosofo 1: Agarrando palillo derecho.");
		pthread_mutex_unlock(&mutex);
		//Estado comiendo
		printf("\nSoy el filosofo 1: Comiendo...");
		do_nothing(4,"\nSoy el filosofo 1: Provecho!");	
		i++; //Incremento en 1 para que coma los platos que quiere segun el usuario	
		//Estado dejando palillo izquierdo
		sem_post(&P1);
		printf("\nSoy el filosofo 1: Dejando el palillo izquierdo.");
		//Estado dejando palillo derecho
		sem_post(&P2);
		printf( "\nSoy el filosofo 1: Dejando el palillo derecho.");
	}
}

static void* Filosofo2 (void* platos){ 
	
	int P = *((int *) platos); //int P representa la cantidad de platos que va a comer el filosofo
	int i = 0; //Comienzo desde cero hasta n para repetir el proceso
	while (i < P){
		//Estado pensando
		printf("\nSoy el filosofo 2: Pensando...");
		do_nothing(2,"\nSoy el filosofo 2: Eureka!");
		//Sección crítica: Cuando agarra palillos
		//Estado agarrando palillo izquierdo
		pthread_mutex_lock(&mutex);
		sem_wait(&P3);
		printf("\nSoy el filosofo 2: Agarrando palillo izquierdo.");
		pthread_mutex_unlock(&mutex);
		//Estado agarrando palillo derecho
		pthread_mutex_lock(&mutex);
		sem_wait(&P2);
		printf("\nSoy el filosofo 2: Agarrando palillo derecho.");
		pthread_mutex_unlock(&mutex);
		//Estado comiendo
		printf("\nSoy el filosofo 2: Comiendo...");
		do_nothing(4,"\nSoy el filosofo 2: Provecho!");	
		i++; //Incremento en 1 para que coma los platos que quiere segun el usuario	
		//Estado dejando palillo izquierdo
		sem_post(&P3);
		printf("\nSoy el filosofo 2: Dejando el palillo izquierdo.");
		//Estado dejando palillo derecho
		sem_post(&P2);
		printf("\nSoy el filosofo 2: Dejando el palillo derecho.");
	}
}

static void* Filosofo3 (void* platos){ //int n representa la cantidad de platos que va a comer el filosofo
	
	int P = *((int *) platos);
	int i = 0; //Comienzo desde cero hasta n para repetir el proceso
	while (i < P){
		//Estado pensando
		printf("\nSoy el filosofo 3: Pensando...");
		do_nothing(2,"\nSoy el filosofo 3: Eureka!");
		//Sección crítica: Cuando agarra palillos
		//Estado agarrando palillo izquierdo
		pthread_mutex_lock(&mutex);
		sem_wait(&P4);
		printf("\nSoy el filosofo 3: Agarrando palillo izquierdo.");
		pthread_mutex_unlock(&mutex);
		//Estado agarrando palillo derecho
		pthread_mutex_lock(&mutex);
		sem_wait(&P3);
		printf("\nSoy el filosofo 3: Agarrando palillo derecho.");
		pthread_mutex_unlock(&mutex);
		//Estado comiendo
		printf("\nSoy el filosofo 3: Comiendo...");
		do_nothing(4,"\nSoy el filosofo 3: Provecho!");
		i++; //Incremento en 1 para que coma los platos que quiere segun el usuario	
		//Estado dejando palillo izquierdo
		sem_post(&P4);
		printf("\nSoy el filosofo 3: Dejando el palillo izquierdo.");
		//Estado dejando palillo derecho
		sem_post(&P3);
		printf("\nSoy el filosofo 3: Dejando el palillo derecho.");
	}
}

static void* Filosofo4 (void* platos){ //int n representa la cantidad de platos que va a comer el filosofo
	
	int P = *((int *) platos);
	int i = 0; //Comienzo desde cero hasta n para repetir el proceso
	while (i < P){
		//Estado pensando
		printf("\nSoy el filosofo 4: Pensando...");
		do_nothing(2,"\nSoy el filosofo 4: Eureka!");
		//Sección crítica: Cuando agarra palillos
		//Estado agarrando palillo izquierdo
		pthread_mutex_lock(&mutex);
		sem_wait(&P5);
		printf("\nSoy el filosofo 4: Agarrando palillo izquierdo.");
		pthread_mutex_unlock(&mutex);
		//Estado agarrando palillo derecho
		pthread_mutex_lock(&mutex);
		sem_wait(&P4);
		printf("\nSoy el filosofo 4: Agarrando palillo derecho.");
		pthread_mutex_unlock(&mutex);
		//Estado comiendo
		printf("\nSoy el filosofo 4: Comiendo...");
		do_nothing(4,"\nSoy el filosofo 4: Provecho!");
		i++; //Incremento en 1 para que coma los platos que quiere segun el usuario
		//Estado dejando palillo izquierdo
		sem_post(&P5);
		printf("\nSoy el filosofo 4: Dejando el palillo izquierdo.");
		//Estado dejando palillo derecho
		sem_post(&P4);
		printf("\nSoy el filosofo 4: Dejando el palillo derecho.");
	}
}

static void* Filosofo5 (void* platos){ //int n representa la cantidad de platos que va a comer el filosofo
	
	int P = *((int *) platos);
	int i = 0; //Comienzo desde cero hasta n para repetir el proceso
	while (i < P){
		//Estado pensando
		printf("\nSoy el filosofo 5: Pensando...");
		do_nothing(2,"\nSoy el filosofo 5: Eureka!");
		//Sección crítica: Cuando agarra palillos
		//Estado agarrando palillo izquierdo
		pthread_mutex_lock(&mutex);
		sem_wait(&P1);
		printf("\nSoy el filosofo 5: Agarrando palillo izquierdo.");
		pthread_mutex_unlock(&mutex);
		//Estado agarrando palillo derecho
		pthread_mutex_lock(&mutex);
		sem_wait(&P5);
		printf("\nSoy el filosofo 5: Agarrando palillo derecho.");
		pthread_mutex_unlock(&mutex);
		//Estado comiendo
		printf("\nSoy el filosofo 5: Comiendo...");
		do_nothing(4,"\nSoy el filosofo 5: Provecho!");
		i++; //Incremento en 1 para que coma los platos que quiere segun el usuario
		//Estado dejando palillo izquierdo
		sem_post(&P1);
		printf("\nSoy el filosofo 5: Dejando el palillo izquierdo.");
		//Estado dejando palillo derecho
		sem_post(&P5);
		printf("\nSoy el filosofo 5: Dejando el palillo derecho.");
	}
}

int main(){
	
	printf("Cuantos platos desea comer? ");
	scanf("%d", &P);
	pthread_mutex_init(&mutex, NULL);
	pthread_t f1, f2, f3, f4, f5;
	sem_init(&P1, 0, 1);
	sem_init(&P2, 0, 1);
	sem_init(&P3, 0, 1);
	sem_init(&P4, 0, 1);
	sem_init(&P5, 0, 1);
	pthread_create(&f1, NULL, *Filosofo1, (void*) &P);
	pthread_create(&f2, NULL, *Filosofo2, (void*) &P);
	pthread_create(&f3, NULL, *Filosofo3, (void*) &P);
	pthread_create(&f4, NULL, *Filosofo4, (void*) &P);
	pthread_create(&f5, NULL, *Filosofo5, (void*) &P);
	sem_destroy(&P1);
	sem_destroy(&P2);
	sem_destroy(&P3);
	sem_destroy(&P4);
	sem_destroy(&P5);
	pthread_exit(NULL);
	return 0;
}